package com.example.bugtracker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SpringBootApplication
public class BugTrackerApplication implements WebMvcConfigurer {

    public static void main(String[] args) {
        SpringApplication.run(BugTrackerApplication.class, args);
    }

}
