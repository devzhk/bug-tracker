package com.example.bugtracker.model;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.util.Date;

@Entity
public class Report {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @NotEmpty(message = "Summary is required!")
    private String summary;

    @NotEmpty(message = "Description is required!")
    private String description;

    @NotEmpty(message = "Reporter is required!")
    private String reporter;

    @NotEmpty(message = "Assignee is required!")
    private String assignee;

    @DateTimeFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    private Date createdAt;

    private String status;

    @NotEmpty
    private String priority;

    private String filename;

    public Report() {
    }

    public Report(@NotEmpty(message = "Summary is required!") String summary, @NotEmpty(message = "Description is required!") String description, @NotEmpty(message = "Reporter is required!") String reporter, @NotEmpty(message = "Assignee is required!") String assignee, Date createdAt, String status, @NotEmpty String priority) {
        this.summary = summary;
        this.description = description;
        this.reporter = reporter;
        this.assignee = assignee;
        this.createdAt = createdAt;
        this.status = status;
        this.priority = priority;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getReporter() {
        return reporter;
    }

    public void setReporter(String reporter) {
        this.reporter = reporter;
    }

    public String getAssignee() {
        return assignee;
    }

    public void setAssignee(String assignee) {
        this.assignee = assignee;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }
}
