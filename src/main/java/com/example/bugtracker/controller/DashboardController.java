package com.example.bugtracker.controller;

import com.example.bugtracker.repos.ReportRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/dashboard")
public class DashboardController {
    private final ReportRepository repository;

    public DashboardController(ReportRepository repository) {
        this.repository = repository;
    }

    @GetMapping
    public String openDashboard(Model model){
        model.addAttribute("reports", repository.findAll());
        return "dashboard";
    }
}
