package com.example.bugtracker.controller;

import com.example.bugtracker.repos.UserRepository;
import com.example.bugtracker.model.Role;
import com.example.bugtracker.model.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.util.Collections;

@Controller
@RequestMapping("/registration")
public class RegistrationController {
    private final UserRepository userRepository;

    public RegistrationController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @GetMapping
    private String openRegisterPage(Model model){
        User user = new User();
        model.addAttribute("user", user);
        return "registration";
    }

    @PostMapping
    private String addUser(@Valid User user, BindingResult bindingResult, Model model) {
        User userFromDb = userRepository.findByUsername(user.getUsername());

        if (bindingResult.hasErrors()){
            return "registration";
        }

        if (userFromDb != null) {
            model.addAttribute("message", "User exist!");
            return "registration";
        }

        user.setActive(true);
        user.setRoles(Collections.singleton(Role.USER));
        userRepository.save(user);

        return "redirect:/login";
    }
}
