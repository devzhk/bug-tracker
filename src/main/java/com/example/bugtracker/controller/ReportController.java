package com.example.bugtracker.controller;

import com.example.bugtracker.model.Report;
import com.example.bugtracker.model.User;
import com.example.bugtracker.repos.ReportRepository;
import com.example.bugtracker.repos.UserRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Controller
@RequestMapping("/report")
public class ReportController {
    private final ReportRepository reportRepository;
    private final UserRepository userRepository;

    public ReportController(ReportRepository reportRepository, UserRepository userRepository) {
        this.reportRepository = reportRepository;
        this.userRepository = userRepository;
    }

    @Value("${upload.path}")
    private String uploadPath;

    @GetMapping("/create")
    public String reportBug(Model model) {
        List<User> userList = userRepository.findAll();
        model.addAttribute(userList);
        return "createBugReport";
    }

    @PostMapping("/create")
    public String submitReport(
            Report report,
            @RequestParam("file") MultipartFile file
    ) throws IOException {
        if (file!=null){

            File uploadDir = new File(uploadPath);

            if (!uploadDir.exists()){
                uploadDir.mkdir();
            }

            String uiid = UUID.randomUUID().toString();
            String resultFileName = uiid + "." + file.getOriginalFilename();

            file.transferTo(new File(uploadPath + "/" + resultFileName));
            report.setFilename(resultFileName);
        }
        report.setStatus("New");
        report.setCreatedAt(new Date());
        reportRepository.save(report);
        return "redirect:/dashboard";
    }

    @PostMapping("/edit")
    public String editReport(Report report) {
        try {
            report.setCreatedAt(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(report.getCreatedAt().toString()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        reportRepository.save(report);
        return "redirect:/dashboard";
    }

    @GetMapping("/view")
    public String viewReport(@RequestParam("id") Integer id, Model model) {
        Report report = reportRepository.findById(id);
        List<User> userList = userRepository.findAll();
        model.addAttribute("list", userList);
        model.addAttribute("report", report);
        return "viewBugReport";
    }

    @GetMapping("/edit")
    public String reportBug(@RequestParam("id") Integer id, Model model) {
        Report report = reportRepository.findById(id);
        List<User> userList = userRepository.findAll();
        model.addAttribute("list", userList);
        model.addAttribute("report", report);
        return "editBugReport";
    }
}
