package com.example.bugtracker.repos;

import com.example.bugtracker.model.Report;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;


public interface ReportRepository extends CrudRepository<Report, Long> {

    Report findById(@Param("id") Integer id);


}
