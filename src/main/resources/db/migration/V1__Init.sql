create table hibernate_sequence
(
  next_val bigint
);

insert into hibernate_sequence
values (1);

create table report
(
  id          integer not null,
  assignee    varchar(255),
  created_at  datetime,
  description varchar(255),
  filename    varchar(255),
  priority    varchar(255),
  reporter    varchar(255),
  status      varchar(255),
  summary     varchar(255),
  primary key (id)
);

create table user_roles
(
  user_id bigint not null,
  roles   varchar(255)
);

create table usr
(
  id       bigint not null,
  active   bit    not null,
  password varchar(255),
  username varchar(255),
  primary key (id)
);

alter table user_roles
  add constraint FKg6agnwreityp2vf23bm2jgjm3 foreign key (user_id) references usr (id);
