alter table usr
  add column first_name varchar(255);

alter table usr
  add column last_name varchar(255);

alter table usr
  add column email varchar(255);